from flask import url_for

from imgcrawler import app
import imgcrawler.scraper as scraper

import json
import pytest


@pytest.fixture(scope="session")
def new_app(request):
    app.config['SERVER_NAME'] = "localhost"
    app.config['DEBUG'] = True
    ctx = app.app_context()
    ctx.push()

    def release_context():
        ctx.pop()

    request.addfinalizer(release_context)
    return app

@pytest.fixture(scope='session')
def client(new_app):
    return new_app.test_client()


def test_base_url():
    url = "https://github.com/whatever/whaterver/aslkfdjl"

    new_url = scraper.base_url(url)

    assert new_url == "https://github.com"


def test_add_path():
    images = ["/image1.png", "http://www.hello.com/image3/image2.png"]
    url = "http://www.google.com/?images=whatever"

    fq_images = scraper.add_path(url, images)

    assert fq_images == ["http://www.google.com" + images[0], images[1]]


def test_qualify_name():
    original_url = "https://www.google.com/not/root/location"
    base_url = "https://www.google.com"

    image = "/image1.png"
    assert scraper.qualify_name(base_url, original_url, image) \
        == "https://www.google.com/image1.png"

    image = "../../image1.png"
    assert scraper.qualify_name(base_url, original_url, image) \
        == original_url + "/../../image1.png"

    image = "www.hello.com/image1.png"
    assert scraper.qualify_name(base_url, original_url, image) \
        == "http://www.hello.com/image1.png"

    image = "//www.hello.com/image1.png"
    assert scraper.qualify_name(base_url, original_url, image) \
        == "http://www.hello.com/image1.png"

    image = "image1.png"
    assert scraper.qualify_name(base_url, original_url, image) \
        == original_url + "/image1.png"


def test_image_filtering():
    images = ["image.png", "image.jpg", "image.fkx", "image.xrt"]

    assert scraper.filter_images(images, ['png', 'jpg']) == images[0:2]


def test_bad_request(client):
    headers = {"Content-Type": "application/json"}
    response = client.post(url_for('create_job'), headers=headers)
    assert response.status_code == 400

    response = client.post(url_for('create_job'), headers=headers,
                           data=json.dumps({"url": []}))
    assert response.status_code == 400

    response = client.post(url_for('create_job'), headers=headers,
                           data=json.dumps({"urls": []}))
    assert response.status_code == 422

    response = client.post(url_for('create_job'), headers=headers,
                           data=json.dumps({"urls": ["www.google.com"]}))
    assert response.status_code == 400


def test_job_exists(client):
    response = client.get(url_for('job_results', job_id="madeup"))
    assert response.status_code == 404

    response = client.get(url_for('job_status', job_id="madeup"))
    assert response.status_code == 404


def test_run_job(client):
    url = url_for('create_job')
    headers = {"Content-Type": "application/json"}
    data = {'urls': ['http://redis.io']}

    response = client.post(url, data=json.dumps(data), headers=headers)
    assert response.status_code == 202

    json_data = json.loads(response.data)
    job_id = json_data['id']
    assert job_id != ""

    response = client.get(url_for('job_status', job_id=job_id))
    assert response.status_code == 200
    json_data = json.loads(response.data)
    print json_data
    assert json_data['id'] == job_id
    assert type(json_data['status']['completed']) is int
    assert type(json_data['status']['inprogress']) is int

    response = client.get(url_for('job_results', job_id=job_id))
    assert response.status_code == 200
    json_data = json.loads(response.data)
    assert json_data['id'] == job_id
    assert type(json_data['results']) is dict
