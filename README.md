# imgcrawler

A simple web scraper that grabs image links from a given URL. The scraper also travels to every link on the given URL and stores those images as well.

`imgcrawler` is accessed here:

http://imgcrawler.herokuapp.com


## Setup

Create a new virtualenv for project and run:

```shell
pip install -r requirements.txt
```


## Run tests

To run unit tests:

```shell
py.test test.py
```

To test production service:

```shell
python test_prod.py http://statuspage.io http://redis.io
```


## Design decisions

To build `imgcrawler` I decided to launch a background job as a python thread which communicated with redis with the results of its scraping. This allowed the web service to check on the job status/results by querying redis. The flow of a job request is as follows:

* Receive job with a list of URLs

* Validate request and launch scraping job with URL list

* Return unique id of job to user

* Scrape URL and all URLs on top level pages

* Add completed/in-progress status to redis hash using unique job id

* Reconstruct image URLs into fully qualified URLs using some heuristics

* Add image URL results to redis using unique job id


This allows for easy communication between the job and webservice without having to worry about thread communication.

I decided to write my own simple scraper instead of using a library like `scrapy` because the rule set is straightforward and I did not want to introduce such a large dependency for something that is easily written in python. If the web scraper became much more complicated I would reevaluate that decision.


## Improvements

While this implementation is straightforward and easy to understand there are numerous performance improvements that could be made. Here are a few:

* Cache results for x hours or days so that if two users request the same URL we are not doing duplicate crawling

* Better concurrency: We should be able to spawn a thread for every link we want to search and then return the images and stitch them together.

* Use a work queue to add jobs to so that we can independently scale up scraper workers and just throw the work on the queue from the web service.

* Share a single redis connection for entire web instance.

Additionally, the URL reconstruction heuristics could be made better by a more comprehensive rule set or some sort of learning algorithm (if you really wanted to get fancy).

Python is a shortcoming in this scenario due to the GIL. If I was to spend more than a day on this project I would most likely pick a different language for the web scraping that has a better concurrency story (Go, Java, etc.). We could easily still use the webserver portion but extract out the scraper into a worker program that reads jobs off of a queue.

An outline of the more performant worker architecture:

* Keep flask webservice

* Put tuple of (unqiue id, [urls]) onto work queue

* Workers grab jobs off of work queue and report status and results to redis as before

* Flask webservice communicates with redis exactly the same to get status and results


