""" Test production deployment of url scraper

Usage:

python test_prod.py http://google.com http://statuspage.io ...

"""

import requests
import json
import time
import sys

def test_job(urls, seconds_to_poll):
    url = "http://imgcrawler.herokuapp.com"

    # Create job
    headers = {"Content-Type": "application/json"}
    data = {"urls": urls}
    response = requests.post(
        url + "/jobs",
        data=json.dumps(data),
        headers=headers
    )
    print "Created job:"
    print response.json()

    # Poll status every second
    for i in range(seconds_to_poll):
        response = requests.get(url + "/jobs/"+response.json()['id']+"/status")
        print "Status:"
        print response
        print response.json()
        time.sleep(1)

    # Print result
    response = requests.get(url + "/jobs/"+response.json()['id']+"/results")
    print "Results:"
    print response
    print response.json()

if __name__ == "__main__":
    urls = sys.argv[1:]
    test_job(urls, 10)
