from urlparse import urlparse
from lxml import html
import requests
import logging

from .persistance import Persistance

class ScrapeJob(object):

    def __init__(self, unique_id, urls):
        self.urls = urls
        self.unique_id = unique_id

        self.db = Persistance(self.unique_id)
        self.db.setup_values(urls)

    def start_job(self):
        for url in self.urls:
            self.db.incr_inprogress(1)
            self.get_image_set(url)

    def get_image_set(self, url):
        """ Get all images for url and all the images following
        all of the links in url.
        """
        links = self.extract_data(url)
        self.db.incr_inprogress(len(links))

        for link in links:
            self.db.add_url(link)
            self.extract_data(link)

    def extract_data(self, current_url):
        """ Extract all of the images from current url and store
        them in redis.
        """
        try:
            links, images = scrape_site(current_url)
            self.db.incr_completed()
            self.db.add_images(current_url, images)
            return links
        # Catch bad links that we scraped
        except requests.exceptions.InvalidSchema:
            logging.error("Invalid schema for link %s", current_url)
            return []

    def __repr__(self):
        return "<ScrapeJob id={} inprogress={} completed={}>".format(
            self.unique_id,
            self.db.get_inprogress(),
            self.db.get_completed()
        )


def scrape_site(url):
    page = requests.get(url)
    tree = html.fromstring(page.content)

    images = tree.xpath('//img/@src')
    images = add_path(url, images)

    links = tree.xpath('//a/@href')
    links = add_path(url, links)

    return (links, images)


def add_path(url, items):
    """ Update image paths to have fully qualified
    names if they don't already.
    """
    new_url = base_url(url)
    fully_qualified_uri = []

    for item in items:
        if len(item) > 0:
            qualified_name = qualify_name(new_url, url, item)
            fully_qualified_uri.append(qualified_name)

    return fully_qualified_uri


def base_url(url):
    parsed = urlparse(url)
    return "{url.scheme}://{url.netloc}".format(url=parsed)


def qualify_name(root_url, original_url, item):
    """ Fix url in special cases that we may encounter """
    parsed_item = urlparse(item)

    if parsed_item.scheme == "":

        if item[:2] == "//":
            return "http:" + item

        elif item[:3] == "www":
            return "http://" + item

        elif item[0] == "/":
            return root_url + item

        elif item[-1] == "/":
            return original_url + item

        return original_url + "/" + item

    return item


def filter_images(images, allowed_types):
    """ Filter images by extenstion type """
    filtered = []

    for image in images:
        image_ext = image.split('.')[-1]
        if image_ext in allowed_types:
            filtered.append(image)

    return filtered
