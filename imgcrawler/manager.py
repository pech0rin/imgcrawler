""" Thread manager for requests.

Inspired by: http://code.activestate.com/recipes/577187-python-thread-pool/

At some point converting this to Celery would make sense.
"""

import Queue
import threading

import logging
import traceback


class ScraperWorker(threading.Thread):

    def __init__(self, job_queue):
        threading.Thread.__init__(self)
        self.job_queue = job_queue
        self.start()

    def run(self):
        while True:
            scrape_job = self.job_queue.get()
            self._run_job(scrape_job)
            self.job_queue.task_done()

    def _run_job(self, scrape_job):
        try:
            scrape_job.start_job()
        except Exception as e:
            logging.error("Exception caught while scraping: %s", e.message)
            logging.error(traceback.format_exc())


class WorkerPool(object):

    def __init__(self, num_workers):
        self.job_queue = Queue.Queue(num_workers)

        for _ in range(num_workers):
            ScraperWorker(self.job_queue)

    def add_job(self, job):
        self.job_queue.put(job)
