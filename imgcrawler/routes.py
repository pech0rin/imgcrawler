from flask import jsonify, abort, request
from urlparse import urlparse

import uuid
import os

from . import app
from .scraper import ScrapeJob
from .persistance import Persistance
from .manager import WorkerPool

worker_pool = WorkerPool(int(os.environ['SCRAPER_THREADS']))


@app.route("/jobs", methods=["POST"])
def create_job():
    """ Create a web scraping job given a list of
    urls.

    Request:

    :param urls: list of urls to scrape


    Response (202):
    Job successfully created

    :param id: unique identifier for a job


    Response (400):
    Invalid request. Missing url parameter.
    Invalid request. URLs malformed.

    Response (422):
    Invalid request. Empty array.

    :param message: error response

    """
    json = request.get_json()
    if 'urls' not in json:
        return client_error(400, "`urls` is a required parameter")

    urls = json['urls']
    if len(urls) == 0:
        return client_error(422, "`urls` must not be empty")

    if invalid_urls(urls):
        return client_error(400, "`urls` must be have a scheme and domain")

    job_id = generate_id()
    job = ScrapeJob(job_id, urls)
    worker_pool.add_job(job)

    response = jsonify(id=job_id)
    response.status_code = 202
    return response


@app.route("/jobs/<job_id>/status", methods=["GET"])
def job_status(job_id):
    """ Return the status of a currently running job.

    Request:
    :param job_id: unique indentifier for job

    Response (200):

    :param id: unique indentifier for job
    :param status:  JSON dictionary containing
      - `completed`: number of pages scraped
      - `inprogess`: number of pages currently being scraped

    Response (404):
    Resouce with job_id not found.

    """
    db = Persistance(job_id)
    if not db.does_exist():
        abort(404)

    return jsonify(**db.get_status())


@app.route("/jobs/<job_id>/results", methods=["GET"])
def job_results(job_id):
    """ Return the image urls found during job.

    Request:
    :param job_id: unique indentifier for job

    Response (200):

    :param id: unique indentifier for job
    :param results:  JSON dictionary mapping urls to image lists

    Response (404):
    Resource with job_id not found.


    """
    db = Persistance(job_id)
    if not db.does_exist():
        abort(404)

    return jsonify(**db.get_results())


def client_error(status, message):
    response = jsonify(message=message)
    response.status_code = status
    return response


def invalid_urls(urls):
    """ Use urlparse to test if all urls are
    well-formed.
    """
    for url in urls:
        parsed = urlparse(url)
        if parsed.scheme == '' or parsed.netloc == '':
            return True
    return False


def generate_id():
    return str(uuid.uuid4()).replace("-", "")
