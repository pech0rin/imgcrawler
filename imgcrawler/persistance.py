from redis import StrictRedis
import os


class Persistance(object):
    """ Handle persistance for scraping jobs.

    Data organization:
    Hash = job:<job_id>
    Values = 
        completed = 1
        inprogress = 2

    job:<job_id>:urls = url1,url2,url3
    job:<job_id>:url:<url_name> = fbc.com/imag12.png,...

    """
    COMPLETED = "completed"
    INPROGRESS = "inprogress"
    URLS = "urls"

    def __init__(self, job_id):
        redis_url = os.environ['REDIS_URL']
        self.redis = StrictRedis.from_url(redis_url)
        self.job_id = job_id
        self.job_hash = self.get_hash(job_id)

    def setup_values(self, urls):
        self.redis.hmset(
            self.job_hash,
            {
                self.COMPLETED: 0,
                self.INPROGRESS: 0,
                self.URLS: " ".join(urls)
            }
        )

    def does_exist(self):
        return self.redis.exists(self.job_hash)

    def get_results(self):
        """ Generate serializable result mappings urls to image links """
        results = {url: list(self.get_images(url))
                   for url in self.get_urls()}
        return {
            "id": self.job_id,
            "results": results
        }

    def get_status(self):
        """ Generate serializable status for job """
        return {
            "id": self.job_id,
            "status": {
                "completed": int(self.get_completed()),
                "inprogress": int(self.get_inprogress())
            }
        }

    def incr_completed(self):
        self.redis.hincrby(self.job_hash, self.COMPLETED, 1)
        self.incr_inprogress(-1)

    def incr_inprogress(self, value):
        self.redis.hincrby(self.job_hash, self.INPROGRESS, value)

    def get_completed(self):
        return self.redis.hget(self.job_hash, self.COMPLETED)

    def get_inprogress(self):
        return self.redis.hget(self.job_hash, self.INPROGRESS)

    def add_url(self, url):
        urls = self._get_urls()
        urls += " " + url
        self.redis.hset(self.job_hash, self.URLS, urls)

    def get_urls(self):
        return self._get_urls().split(" ")

    def _get_urls(self):
        return self.redis.hget(self.job_hash, self.URLS)

    def add_images(self, url, images):
        for image in images:
            self.add_image(url, image)

    def add_image(self, url, image):
        url_key = self.get_url_key(url)
        self.redis.sadd(url_key, image)

    def get_images(self, url):
        url_key = self.get_url_key(url)
        return self.redis.smembers(url_key)

    def get_url_key(self, url):
        return self.job_hash + ":" + self.get_url_hash(url)

    def get_url_hash(self, url):
        return "url:" + url

    def get_hash(self, job_id):
        return "job:{}".format(job_id)
